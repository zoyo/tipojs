/**
 * Api utilitária para tratar tipos em javascript.
 * 
 * Composta pelos métodos:
 * 		jsTypes.getType     -> Retorna o tipo do objeto informado;
 * 		jsTypes.isString    -> Verifica se é string;
 * 		jsTypes.isNumber    -> Verifica se é número;
 * 		jsTypes.isObject    -> Verifica se é objeto javascript;
 * 		jsTypes.isArray     -> Verifica se é array;
 * 		jsTypes.isBoolean   -> Verifica se é boolean;
 * 		jsTypes.isUndefined -> Verifica se é undefined (ou null).
 * 
 * Mais detalhes nas funções.
 */
var jsTypes = (function(){
	/**
	 * Retorna o tipo do objeto javascript informado.
	 * 
	 * @param obj Objeto que deve ser verificado para retornar o tipo.
	 * 
	 * @return String contendo o tipo do objeto informado (number, string, function, object, array, etc...).
	 * 
	 * @example
	 * 		jsTypes.getType('texto'); // 'string'
	 * 		jsTypes.getType(null);    // 'undefined'
	 */
	var _getType = function(obj){
		if(obj == null) {return 'undefined'}
		
		return Object.prototype.toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
	}
	
	/**
	 * Verifica se o objeto informado é uma string.
	 * 
	 * @param obj Objeto que deve ser verificado.
	 * 
	 * @return true se o obj for uma string ou false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isString('texto'); // true
	 * 		jsTypes.isString('234');   // true
	 * 		jsTypes.isString(234);     // false
	 */
	var _isString = function(obj){
		return _getType(obj) === 'string';
	}
	
	/**
	 * Verifica se o objeto informado é um número.<br>
	 * Por padrão, uma string, mesmo com conteúdo númerico, retorna false, mas esse
	 * comportamento pode ser alterado, passando-se true (ou a constante jsTypes.CONSIDER_STRING)
	 * no segundo parametro. 
	 * 
	 * @param obj Objeto que deve ser verificado.
	 * @param considerNumberInString Parametro opcional que permite considerar ou não uma string com conteúdo numérico.
	 * 
	 * @return true se o obj for um número ou, se necessário, uma string numérica e false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isNumber(123);                              // true
	 * 		jsTypes.isNumber('123');                            // false
	 * 		jsTypes.isNumber('123', true);                      // true
	 * 		jsTypes.isNumber('10.04', jsTypes.CONSIDER_STRING); // true 
	 * 		jsTypes.isNumber('10.04', false);                   // false 
	 */
	var _isNumber = function(obj, considerNumberInString){
		var _verStr = considerNumberInString || false;
		
		if(_getType(obj) === 'number'){
			return true
		}
		
		return _verStr && _getType(obj) === 'string' && _isNum(obj);
	}
	
	var _isNum = function(obj) {
		return !isNaN(obj);
	}
	
	/**
	 * Verifica se o objeto informado é um objeto javascript.
	 * 
	 * @param obj Objeto que deve ser verificado.
	 * 
	 * @return true se obj for um objeto javascript, false caso contrário.
	 * 
	 * @example 
	 * 		jsTypes.isObject({});                // true
	 * 		jsTypes.isObject([1, 2, 3]);         // false
	 * 		jsTypes.isObject({'chave':'valor'}); // true
	 */
	var _isObject = function(obj){
		return _getType(obj) == 'object';
	}
	
	/**
	 * Verifica se o objeto informado é um array.
	 * 
	 * @param obj Objeto que deve ser verificado.
	 * 
	 * @return true se obj for um array ou false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isArray('array');   // false
	 * 		jsTypes.isArray([]);        // true
	 * 		jsTypes.isArray([1, 2, 3]); // true
	 */
	var _isArray = function(obj){
		return _getType(obj) === 'array';
	}
	
	
	/**
	 * Verifica se o objeto informado é um boolean. <br>
	 * Por padrão, uma string, mesmo com os conteúdos 'true' e 'false' retorna false, mas esse
	 * comportamento pode ser alterado passando true (ou a constante jsTypes.CONSIDER_STRING)
	 * no segundo parametro.<br>
	 * Importante observar que, se considerStringTrueFalse for true, a função retornará verdadeiro
	 * para ambas strings 'true' e 'false'.
	 * 
	 * @param obj Objeto que deve ser verificado.
	 * @param considerStringTrueFalse Parametro opcional que permite considerar ou não uma string com conteúdo booleano.
	 * 
	 * @return true se obj for boolean ou, se necessário, obj for 'true' ou 'false' e false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isBoolean(true);                            // true
	 * 		jsTypes.isBoolean(false);                           // true
	 * 		jsTypes.isBoolean(1);                               // false
	 * 		jsTypes.isBoolean('2');                             // false
	 * 		jsTypes.isBoolean('true');                          // false
	 * 		jsTypes.isBoolean('false');                         // false
	 * 		jsTypes.isBoolean('true', true);                    // true
	 * 		jsTypes.isBoolean('false', jsType.CONSIDER_STRING); // true
	 */
	var _isBoolean = function(obj, considerStringTrueFalse){
		var _verStr = considerStringTrueFalse || false;
		
		if(_getType(obj) === 'boolean'){
			return true;
		}
		
		return _verStr && _getType(obj) === 'string' && 'true|false'.indexOf(obj.toLowerCase()) > -1;
	}
	
	/**
	 * Verifica se obj é uma função.
	 * 
	 * @param obj Objeto a ser verificado.
	 * 
	 * @return true se obj for uma função e false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isFunction('function');   // false
	 * 		jsTypes.isFunction(function(){}); // true
	 */
	var _isFunction = function(obj){
		return _getType(obj) === 'function';
	}
	
	/**
	 * Verifica se obj é null ou undefined.
	 * 
	 * @param obj Objeto a ser verificado.
	 * 
	 * @return true se obj for null ou undefined e false caso contrário.
	 * 
	 * @example
	 * 		jsTypes.isUndefined();     // true
	 * 		jsTypes.isUndefined(null); // true
	 * 		jsTypes.isUndefined('2');  // false
	 */
	var _isUndefined = function(obj){
		return _getType(obj) === 'undefined';
	}
	
	return {
		CONSIDER_STRING : true,
		
		getType    : _getType,
		isString   : _isString,
		isNumber   : _isNumber,
		isObject   : _isObject,
		isArray    : _isArray,
		isBoolean  : _isBoolean,
		isUndefined: _isUndefined
	}
})();